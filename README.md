## Modular Fab Stepper Driver

This is a Fab-Labbable (i.e. easy-to-pcb-mill) design for a networked stepper driver. Check [the dev log](log/fab-step-log.md) for more details.

![motor](log/images/2021-11-22_fab-step.jpg)

This uses the SAMD21E18 microcontroller, 2x A4950 H-Bridges to drive, and is connected to [osap networked controllers](https://gitlab.cba.mit.edu/jakeread/osap) using the [uart-clocked bus data link](https://gitlab.cba.mit.edu/jakeread/ucbus-notes), an RS-485 powered deterministic bus. 

![route](log/images/2021-06-19_routed.png)
![schem](log/images/2021-06-19_schem.png)

### Building Fab-Step 

Circuit files are available [here](2021-06_fab-step-ucbus). 

![labels](log/images/2022-01-05_fab-step-labels.png) 

### BOM 

I have published an online [Bill of Materials, here](https://docs.google.com/spreadsheets/d/e/2PACX-1vS1O6LhY8GtTYtnFfkABpFL04ExAnMShsRPDNRNZeFXVBi_aEPuFjofpvHQqx7KgcQ-_qJOgMHmKk_m/pubhtml) or you can download the [spreadsheet](2021-06_fab-step-ucbus/fab-step-ucbus-bom.xlsx) or [csv](2021-06_fab-step-ucbus/fab-step-ucbus-bom.csv). 

### Firmware 

Firmware for the motor driver is [here](firmware) and uses platform.io to run, see [this guide](https://mtm.cba.mit.edu/2021/2021-10_microcontroller-primer/fab-platformio/) to build and load code. 

### Network Hookup 

The fab-step receives power and data from a 10-pin IDC connector, using a protocol I have developed [here: the UART-Clocked Bus](https://gitlab.cba.mit.edu/jakeread/ucbus-notes). 