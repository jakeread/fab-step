// vertex config for D21... 

#ifndef VERTEX_CONFIG_H_
#define VERTEX_CONFIG_H_

#define VT_SLOTSIZE 256
#define VT_STACKSIZE 2  // must be >= 2 for ringbuffer operation 
#define VT_MAXCHILDREN 16

#endif 