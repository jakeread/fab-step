/*
drivers/step_a4950.h

stepper code for two A4950s w/ VREF via TC -> RC Filters 

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2021

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the OSAP project. 
Copyright is retained and must be preserved. The work is provided as
is; no warranty is provided, and users accept all liability.
*/

#ifndef STEP_A4950_H_
#define STEP_A4950_H_

#include <Arduino.h> 

// lol 
#define MICROSTEP_64_COUNT 1
#define MICROSTEP_32_COUNT 1
#define MICROSTEP_16_COUNT 1
#define MICROSTEP_8_COUNT 2 
#define MICROSTEP_4_COUNT 4
#define MICROSTEP_2_COUNT 8
#define MICROSTEP_1_COUNT 16 

// AIN1 PA14
// AIN2 PA15 
// BIN1 PA05 
// BIN2 PA06 
#define AIN1_PIN 14
#define AIN1_PORT PORT->Group[0]
#define AIN1_BM (uint32_t)(1 << AIN1_PIN)
#define AIN2_PIN 15
#define AIN2_PORT PORT->Group[0]
#define AIN2_BM (uint32_t)(1 << AIN2_PIN)
#define BIN1_PIN 5 
#define BIN1_PORT PORT->Group[0]
#define BIN1_BM (uint32_t)(1 << BIN1_PIN)
#define BIN2_PIN 6 
#define BIN2_PORT PORT->Group[0] 
#define BIN2_BM (uint32_t)(1 << BIN2_PIN)

// handles
#define AIN1_HI AIN1_PORT.OUTSET.reg = AIN1_BM
#define AIN1_LO AIN1_PORT.OUTCLR.reg = AIN1_BM
#define AIN2_HI AIN2_PORT.OUTSET.reg = AIN2_BM
#define AIN2_LO AIN2_PORT.OUTCLR.reg = AIN2_BM 
#define BIN1_HI BIN1_PORT.OUTSET.reg = BIN1_BM
#define BIN1_LO BIN1_PORT.OUTCLR.reg = BIN1_BM
#define BIN2_HI BIN2_PORT.OUTSET.reg = BIN2_BM
#define BIN2_LO BIN2_PORT.OUTCLR.reg = BIN2_BM

// set a phase up or down direction
// transition low first, avoid brake condition for however many ns 
#define A_UP AIN2_LO; AIN1_HI
#define A_OFF AIN2_LO; AIN1_LO
#define A_DOWN AIN1_LO; AIN2_HI
#define B_UP BIN2_LO; BIN1_HI 
#define B_OFF BIN2_LO; BIN1_LO
#define B_DOWN BIN1_LO; BIN2_HI

// AVREF via PA07 / TCC1-1 
// BVREF via PA04 / TCC0-0

#define AVREF_PIN 7
#define AVREF_PORT PORT->Group[0] 
#define AVREF_BM (uint32_t)(1 << AVREF_PIN)

#define BVREF_PIN 4
#define BVREF_PORT PORT->Group[0] 
#define BVREF_BM (uint32_t)(1 << BVREF_PIN) 

void step_a4950_init(void);
void step_a4950_set_cscale(float scale);
void step_a4950_publish_currents(void);
void step_a4950_set_microstep(uint8_t microstep);
void step_a4950_step();
void step_a4950_dir(boolean dir);

#endif 