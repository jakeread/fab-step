/*
drivers/step_a4950.cpp

stepper code for two A4950s w/ VREF via TC -> RC Filters 

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2021

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo
projects. Copyright is retained and must be preserved. The work is provided as
is; no warranty is provided, and users accept all liability.
*/

#include "step_a4950.h"
#include "../indicators.h"

// LUT, 0-1022, 64 entries, sin w/ 511 at midpoint 
// full sweep of electrical phase is actually 4 'steps' - 
// so making a full step means incrementing 16 times through this LUT, 
// half step is 8, quarter step 4, eighth step 2, sixteenth microstepping is one, fin 
uint16_t LUT_1022[64] = {
    511,561,611,659,707,752,795,835,872,906,936,962,983,1000,1012,1020,
    1022,1020,1012,1000,983,962,936,906,872,835,795,752,707,659,611,561,
    511,461,411,363,315,270,227,187,150,116,86,60,39,22,10,2,
    0,2,10,22,39,60,86,116,150,187,227,270,315,363,411,461,
};
// on init / cscale, we write new values into this thing, which is where
// we actually pull currents from, for the h-bridges 
uint16_t LUT_CURRENTS[64];

// startup stepper hardware 
void step_a4950_init(void){
    // ------------------------------------------ DIR PINS 
    // all of 'em, outputs 
    AIN1_PORT.DIRSET.reg = AIN1_BM;
    AIN2_PORT.DIRSET.reg = AIN2_BM;
    BIN1_PORT.DIRSET.reg = BIN1_BM;
    BIN2_PORT.DIRSET.reg = BIN2_BM;
    // ------------------------------------------ TCC SETUPS 
    // s/o https://blog.thea.codes/phase-shifted-pwm-on-samd/ 
    // unmask the peripheral, 
    PM->APBCMASK.reg |= PM_APBCMASK_TCC0 | PM_APBCMASK_TCC1;
    // route this clk (on 0) to our peripheral 
    GCLK->CLKCTRL.reg =  GCLK_CLKCTRL_CLKEN |       // (?)
                         GCLK_CLKCTRL_GEN_GCLK0 |   // select 0: already generated main clk
                         GCLK_CLKCTRL_ID_TCC0_TCC1; // route to TCC0 / TCC1  
    while(GCLK->STATUS.bit.SYNCBUSY);
    // ------------------------------------------ AVREF TCC 
    // setup TCC1-1 / PA07 
    // set pin as output, 
    AVREF_PORT.DIRSET.reg = AVREF_BM;
    // mux pin onto peripheral, it's peripheral E, and *odd numbered pin* 
    AVREF_PORT.PINCFG[AVREF_PIN].reg |= PORT_PINCFG_PMUXEN;
    AVREF_PORT.PMUX[AVREF_PIN >> 1].reg = PORT_PMUX_PMUXO_E;
    // prescaler: we will probably go very fast, for now I want to see it 
    TCC1->CTRLA.reg |= TCC_CTRLA_PRESCALER_DIV1;    
    TCC1->WAVE.reg = TCC_WAVE_WAVEGEN_NPWM;
    while(TCC1->SYNCBUSY.bit.WAVE);
    TCC1->PER.reg = 128;
    while(TCC1->SYNCBUSY.bit.PER);
    TCC1->CC[1].reg = 0;
    TCC1->CTRLA.bit.ENABLE = 1;
    // ------------------------------------------ BVREF TCC 
    // setup TCC0-0 / PA04
    // set pin as output, 
    BVREF_PORT.DIRSET.reg = BVREF_BM;
    // mux pin onto peripheral, it's peripheral E, and *even numbered pin* 
    BVREF_PORT.PINCFG[BVREF_PIN].reg |= PORT_PINCFG_PMUXEN;
    BVREF_PORT.PMUX[BVREF_PIN >> 1].reg = PORT_PMUX_PMUXE_E;
    // prescaler: we will probably go very fast, for now I want to see it 
    TCC0->CTRLA.reg |= TCC_CTRLA_PRESCALER_DIV1;    
    TCC0->WAVE.reg = TCC_WAVE_WAVEGEN_NPWM;
    while(TCC0->SYNCBUSY.bit.WAVE);
    TCC0->PER.reg = 128;
    while(TCC0->SYNCBUSY.bit.PER);
    TCC0->CC[0].reg = 0;
    TCC0->CTRLA.bit.ENABLE = 1;
    // ------------------------------------------ INIT 
    // now, setup current table, default to 0.0 (disabled).
    step_a4950_set_cscale(0.0F);
    // push a step to init up/down states 
    step_a4950_step();
}

// to set scale, we write a new LUT, this avoids math-in-the-step-tick, 
void step_a4950_set_cscale(float scale){
    // scale max 1.0, min 0.0,
    if(scale > 1.0F) scale = 1.0F;
    if(scale < 0.0F) scale = 0.0F;
    // for each item in the LUTs,
    for(uint8_t i = 0; i < 64; i ++){
        if(LUT_1022[i] > 511){
            // top half, no invert, but shift-down and scale 
            LUT_CURRENTS[i] = (LUT_1022[i] - 511) * 2.0F * scale;
        } else if (LUT_1022[i] < 511){
            // lower half, invert and shift down 
            float temp = LUT_1022[i];   // get lut as float, 
            temp = (temp * -2.0F + 1022) * scale; // scale (flipping) and offset back up 
            LUT_CURRENTS[i] = temp; // set table element, 
        } else {
            // the midpoint: off, 
            LUT_CURRENTS[i] = 0;
        }
    }
    // we need to publish these updates: 
    step_a4950_publish_currents();
}

// step states
boolean _dir = true;                            // current direction 
boolean _dirInvert = false;                     // invert directions ?
uint8_t _microstep_count = MICROSTEP_16_COUNT;  // default to 16th microstepping 
uint8_t lutptra = 16;                           // current pt. in microstep phase 
uint8_t lutptrb = 0;                            // A leads B by 1/4 period 

void step_a4950_set_microstep(uint8_t microstep){
    switch(microstep){
        case 64:
            _microstep_count = MICROSTEP_64_COUNT;
            break;
        case 32:
            _microstep_count = MICROSTEP_32_COUNT;
            break;
        case 16:
            _microstep_count = MICROSTEP_16_COUNT;
            break;
        case 8:
            _microstep_count = MICROSTEP_8_COUNT;
            break;
        case 4: 
            _microstep_count = MICROSTEP_4_COUNT;
            break;
        case 2:
            _microstep_count = MICROSTEP_2_COUNT;
            break;
        case 1:
            _microstep_count = MICROSTEP_1_COUNT;
            break;
        default:
            _microstep_count = MICROSTEP_1_COUNT;
            break;
    }
}

void step_a4950_step(){
    // step LUT ptrs thru table, increment and wrap w/ bit logic 
    if(_dir){
        lutptra += _microstep_count; lutptra = lutptra & 0b00111111;
        lutptrb += _microstep_count; lutptrb = lutptrb & 0b00111111;
    } else {
        lutptra -= _microstep_count; lutptra = lutptra & 0b00111111;
        lutptrb -= _microstep_count; lutptrb = lutptrb & 0b00111111;
    }
    // depending on sign of phase, set up / down on gates 
    if(LUT_1022[lutptra] > 511){
        A_UP;
    } else if (LUT_1022[lutptra] < 511){
        A_DOWN;
    } else {
        A_OFF;
    }
    if(LUT_1022[lutptrb] > 511){
        B_UP;
    } else if (LUT_1022[lutptrb] < 511){
        B_DOWN;
    } else {
        B_OFF;
    }
    step_a4950_publish_currents();
}

void step_a4950_publish_currents(void){
    // set vref currents via these rc flter / pwms 
    TCC1->CC[1].reg = LUT_CURRENTS[lutptra] >> 3;   // TC resolution is 0-128   (7 bit)
    TCC0->CC[0].reg = LUT_CURRENTS[lutptrb] >> 3;   
}
// set direction 
void step_a4950_dir(boolean dir){
    if(_dirInvert){
        _dir = !dir;
    } else {
        _dir = dir;
    }
}