/*
homing.cpp

tiny homing routine for fab-step 

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2022

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the OSAP project. 
Copyright is retained and must be preserved. The work is provided as
is; no warranty is provided, and users accept all liability.
*/

#include "homing.h"
#include "drivers/step_a4950.h"

endpoint_t* _homeStateEP;
uint8_t homeState = HOMESTATE_NONE;
uint32_t homeBackoffStepsTaken = 0;

// home settings 
boolean homeDir = true;
unsigned long lastHomeOperation = 0;
unsigned long homeOperationPeriod = 1000; // in us 
uint32_t homeBackoffDistance = 100;

void homeSetup(endpoint_t* homeStateEP){
    // stash this 
    _homeStateEP = homeStateEP;
    // make an input 
    PORT->Group[LIMIT_PORT].DIRCLR.reg = (1 << LIMIT_PIN);
    PORT->Group[LIMIT_PORT].PINCFG[LIMIT_PIN].bit.INEN = 1;
    // pullup 
    PORT->Group[LIMIT_PORT].OUTSET.reg = (1 << LIMIT_PIN);
}

// return true if limit switch is hit 
boolean limitHit(void){
    return (PORT->Group[LIMIT_PORT].IN.reg & (1 << LIMIT_PIN));
}

void writeHomeSettings(boolean dir, uint32_t stepsPerSecond, uint32_t offset){
    homeDir = dir;
    homeOperationPeriod = 1000000 / stepsPerSecond;
    homeBackoffDistance = offset;
}

uint8_t getHomeState(void){
    return homeState;
}

void startHomingRoutine(void){
    homeState = HOMESTATE_APPROACH;
    endpointWrite(_homeStateEP, &homeState, 1);
}

void runHomingRoutine(void){
  // run this at a rate... 
  if(lastHomeOperation + homeOperationPeriod > micros()) return;
  lastHomeOperation = micros();
  // state switch; 
  switch(homeState){
    case HOMESTATE_NONE:
      break;
    case HOMESTATE_APPROACH: 
      // check for contact, 
      if(limitHit()){
        homeState = HOMESTATE_BACKOFF;
        endpointWrite(_homeStateEP, &homeState, 1);
        homeBackoffStepsTaken = 0;
      } else {
        step_a4950_dir(homeDir);
        step_a4950_step();
      }
      break;
    case HOMESTATE_BACKOFF:
      step_a4950_dir(!homeDir);
      step_a4950_step();
      homeBackoffStepsTaken ++;
      if(homeBackoffStepsTaken > homeBackoffDistance){
        homeState = HOMESTATE_NONE;
        endpointWrite(_homeStateEP, &homeState, 1);
      }
      break;
    default:
      // broken, 
      homeState = HOMESTATE_NONE;
      endpointWrite(_homeStateEP, &homeState, 1);
      break;
  }
}