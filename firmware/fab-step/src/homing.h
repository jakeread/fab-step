/*
homing.h

tiny homing routine for fab-step 

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2022

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the OSAP project. 
Copyright is retained and must be preserved. The work is provided as
is; no warranty is provided, and users accept all liability.
*/

#ifndef HOMING_H_ 
#define HOMING_H_ 

#include <Arduino.h>
#include "osape-d21/osape/osap/endpoint.h"

// limit 
#define LIMIT_PORT 0 
#define LIMIT_PIN 3 

// home states
#define HOMESTATE_NONE 0
#define HOMESTATE_APPROACH 1
#define HOMESTATE_BACKOFF 2

void homeSetup(endpoint_t* homeStateEP);
boolean limitHit(void);
uint8_t getHomeState(void);
void writeHomeSettings(boolean dir, uint32_t stepsPerSecond, uint32_t offset);
void startHomingRoutine(void);
void runHomingRoutine(void);

#endif 