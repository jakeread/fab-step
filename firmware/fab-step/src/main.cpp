#include <Arduino.h>
#include "indicators.h"
#include "config.h"

#include "drivers/step_a4950.h"
#include "homing.h"

#include "osape-d21/osape/osap/osap.h"
#include "osape-d21/vertices/vt_usbSerial.h"
#include "osape-d21/vertices/ucbus/vt_ucBusDrop.h"
#include "osape-d21/vertices/ucbus/ucBusDrop.h"

uint8_t axisPick = 0;
boolean invert = false;

// -------------------------------------------------------- AXIS PICK EP 

EP_ONDATA_RESPONSES onAxisPickData(uint8_t* data, uint16_t len){
  if(data[0] > 3){
    axisPick = 0;
  } else {
    axisPick = data[0];
  }
  return EP_ONDATA_ACCEPT;
}

endpoint_t* axisPickEp = osapBuildEndpoint("axisPick", onAxisPickData);

// -------------------------------------------------------- AXIS INVERSION EP

EP_ONDATA_RESPONSES onAxisInvertData(uint8_t* data, uint16_t len){
  data[0] ? invert = true : invert = false;
  return EP_ONDATA_ACCEPT;
}

endpoint_t* axisInvertEp = osapBuildEndpoint("axisInvert", onAxisInvertData);

// -------------------------------------------------------- MICROSTEP EP 

EP_ONDATA_RESPONSES onMicrostepData(uint8_t* data, uint16_t len){
  step_a4950_set_microstep(data[0]);
  return EP_ONDATA_ACCEPT;
}

endpoint_t* microstepEp = osapBuildEndpoint("microstep", onMicrostepData);

// -------------------------------------------------------- CSCALE DATA

EP_ONDATA_RESPONSES onCScaleData(uint8_t* data, uint16_t len){
  chunk_float32 cscalec = { .bytes = { data[0], data[1], data[2], data[3] } };
  if(cscalec.f > 1.0F){
    cscalec.f = 1.0F;
  } else if (cscalec.f < 0.0F){
    cscalec.f = 0.0F;
  }
  step_a4950_set_cscale(cscalec.f);
  return EP_ONDATA_ACCEPT;
}

endpoint_t* cScaleEp = osapBuildEndpoint("CScale", onCScaleData);

// -------------------------------------------------------- HOME ROUTINE

EP_ONDATA_RESPONSES onHomeData(uint8_t* data, uint16_t len){
  chunk_int32 rate = { .bytes = { data[0], data[1], data[2], data[3] } };
  chunk_uint32 offset = { .bytes = { data[4], data[5], data[6], data[7] } };
  // sign of rate is dir, 
  boolean hdir;
  uint32_t hrate;
  if(rate.i < 0) {
    hdir = false;
    hrate = -1 * rate.i;
  } else {
    hdir = true;
    hrate = rate.i;
  }
  writeHomeSettings(hdir, hrate, offset.u);
  startHomingRoutine();
  return EP_ONDATA_ACCEPT;
}

endpoint_t* homeEp = osapBuildEndpoint("Home", onHomeData);

// -------------------------------------------------------- HOME STATE 

endpoint_t* homeStateEp = osapBuildEndpoint("HomeState");

// -------------------------------------------------------- Homing

// -------------------------------------------------------- SETUP

void setup() {
  CLKLIGHT_SETUP;
  BUSLIGHT_SETUP;
  // debug1pin is the limit pin: we can have one or the other
  //DEBUG1PIN_SETUP;
  homeSetup(homeStateEp);
  // osap setup...
  osapSetup("fab-step");
  vt_usbSerial_setup();
  osapAddVertex(vt_usbSerial);
  vt_ucBusDrop_setup(false, 2);
  osapAddVertex(vt_ucBusDrop);
  // motor API 
  // axis pick 
  osapAddEndpoint(axisPickEp);      // 2
  // axis invert
  osapAddEndpoint(axisInvertEp);    // 3
  // microstep 
  osapAddEndpoint(microstepEp);     // 4
  // cscale 
  osapAddEndpoint(cScaleEp);        // 5
  // homing 
  osapAddEndpoint(homeEp);          // 6 
  osapAddEndpoint(homeStateEp);     // 7 
  // step hardware init 
  step_a4950_init();
}

// -------------------------------------------------------- LOOP 

#define CLK_TICK 250
unsigned long last_tick = 0;

void loop() {
  // do osap things, 
  osapLoop(); 
  //limitHit() ? BUSLIGHT_ON : BUSLIGHT_OFF;
  // do homing things, if need be:
  if(getHomeState() != HOMESTATE_NONE) runHomingRoutine();
  // blink 
  if(millis() > last_tick + CLK_TICK){
    // step_a4950_step();
    last_tick = millis();
    BUSLIGHT_TOGGLE;
  }
}

// -------------------------------------------------------- BUS INTERRUPT / STEP 

uint8_t stepCache[2];
volatile uint8_t cachePtr = 0;
volatile boolean stepValid = false;
volatile uint8_t stepMask = 0;

void ucBusDrop_onPacketARx(uint8_t* inBufferA, volatile uint16_t len){
  // refill step cache:
  stepCache[0] = inBufferA[0]; stepCache[1] = inBufferA[1];
  cachePtr = 0;
  stepValid = true;
}

void ucBusDrop_onRxISR(void){
  // if we dont' have valid steps, bail 
  if(!stepValid) return;
  // if we're currently homing the motor, bail 
  if(getHomeState() != HOMESTATE_NONE) return;
  // extract our step mask
  stepMask = 0b00000011 & (stepCache[cachePtr] >> (axisPick * 2));
  // mask -> step api:
  if(invert){
    (stepMask & 0b00000001) ? step_a4950_dir(true) : step_a4950_dir(false); // dir bit,
  } else {
    (stepMask & 0b00000001) ? step_a4950_dir(false) : step_a4950_dir(true); // dir bit,
  }
  if (stepMask & 0b00000010) step_a4950_step();                           // step bit
  // increment ptr -> next step mask, if over-wrap, steps no longer valid 
  cachePtr ++; if(cachePtr >= 2) stepValid = false;
}